﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ASM2.Models;

namespace ASM2.Controllers
{
    public class ChiNhanhsController : Controller
    {
        private readonly AppDbContext _context;

        public ChiNhanhsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: ChiNhanhs
        public async Task<IActionResult> Index()
        {
              return _context.chiNhanhs != null ? 
                          View(await _context.chiNhanhs.ToListAsync()) :
                          Problem("Entity set 'AppDbContext.chiNhanhs'  is null.");
        }

        // GET: ChiNhanhs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.chiNhanhs == null)
            {
                return NotFound();
            }

            var chiNhanh = await _context.chiNhanhs
                .FirstOrDefaultAsync(m => m.BranchId == id);
            if (chiNhanh == null)
            {
                return NotFound();
            }

            return View(chiNhanh);
        }

        // GET: ChiNhanhs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ChiNhanhs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BranchId,Name,Address,City,State,ZipCode")] ChiNhanh chiNhanh)
        {
            if (ModelState.IsValid)
            {
                _context.Add(chiNhanh);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(chiNhanh);
        }

        // GET: ChiNhanhs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.chiNhanhs == null)
            {
                return NotFound();
            }

            var chiNhanh = await _context.chiNhanhs.FindAsync(id);
            if (chiNhanh == null)
            {
                return NotFound();
            }
            return View(chiNhanh);
        }

        // POST: ChiNhanhs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BranchId,Name,Address,City,State,ZipCode")] ChiNhanh chiNhanh)
        {
            if (id != chiNhanh.BranchId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(chiNhanh);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ChiNhanhExists(chiNhanh.BranchId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(chiNhanh);
        }

        // GET: ChiNhanhs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.chiNhanhs == null)
            {
                return NotFound();
            }

            var chiNhanh = await _context.chiNhanhs
                .FirstOrDefaultAsync(m => m.BranchId == id);
            if (chiNhanh == null)
            {
                return NotFound();
            }

            return View(chiNhanh);
        }

        // POST: ChiNhanhs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.chiNhanhs == null)
            {
                return Problem("Entity set 'AppDbContext.chiNhanhs'  is null.");
            }
            var chiNhanh = await _context.chiNhanhs.FindAsync(id);
            if (chiNhanh != null)
            {
                _context.chiNhanhs.Remove(chiNhanh);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ChiNhanhExists(int id)
        {
          return (_context.chiNhanhs?.Any(e => e.BranchId == id)).GetValueOrDefault();
        }
    }
}
