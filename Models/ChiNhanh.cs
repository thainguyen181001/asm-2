﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace ASM2.Models
{
    public class ChiNhanh
    {
        [Key]
        [Display(Name = "Mã chi nhánh")]
        public int BranchId { get; set; }

        [Display(Name = "Tên chi nhánh")]
        public string Name { get; set; }
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }
        [Display(Name = "Thành phố đặt chi nhánh")]
        public string City { get; set; }
        [Display(Name = "Trạng thái")]
        [RegularExpression(@"(?i)(Active|DeActive)", ErrorMessage = "Only active or deactive")]
        public string State { get; set; }
        [Display(Name = "Mã bưu chính")]
        public string ZipCode { get; set; }

    }
}
