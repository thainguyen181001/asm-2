﻿using Microsoft.EntityFrameworkCore;

namespace ASM2.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet< ChiNhanh > chiNhanhs { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext>options):base(options)
        {

        }
    }
}
